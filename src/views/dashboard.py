import streamlit as st
import pandas as pd
import numpy as np
from authentication import session_handler


@session_handler
def main():
        st.write("# Welcome to the Dashboard")
        chart_data = pd.DataFrame(np.random.randn(20, 3), columns=['a', 'b', 'c'])
        st.line_chart(chart_data)
