import streamlit as st
from authentication import user_authenticated

def main():
    st.write("# Welcome to the Auth App")

    with st.form(key="welcome", clear_on_submit=True):
        with st.sidebar:
            st.markdown("---")
            user = st.text_input("Username", value="", type="password")
            password = st.text_input("Password", value="", type="password")
            submitted = st.form_submit_button("LogIn")

    if submitted:
        result = user_authenticated(username=user, password=password)
        if result:
            st.sidebar.success("Logged in successfully!")

    st.markdown(
        """
        This is a demo multipage app, you need to log in first to see the dashboard view.
        """
    )