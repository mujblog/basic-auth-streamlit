import streamlit as st
import logging
import functools
import typing as t


def add_auth_to_session():
    try:
        st.session_state["authenticated"] = True
    except Exception as e:
        logging.error(e)


def user_authenticated(username: str, password: str):
    if username == "welcome" and password == "1111":
        add_auth_to_session()
        return True
    return False


def session_handler(func: t.Callable) -> t.Callable:
    generic_text = (
        "🚫 __You must have to authenticate yourself "
        "to view the content of the dashbaord. "
        "Please visit the welcome section and authenticate yourself.__"
    )
    @functools.wraps(func)
    def wrapper():
        if 'authenticated' in st.session_state and st.session_state['authenticated']:
            func()
        else:
            st.markdown(generic_text)
    return wrapper